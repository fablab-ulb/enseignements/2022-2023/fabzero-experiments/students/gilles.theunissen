/* 

FILE : code.scad

AUTHOR : Gilles Theunissen <gilles.theunissen@ulb.be>

DATE : 08-03-2023

LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

//*/

//PARAMETERS

$fn = 50;
radius_part = 5.5;
radius_hole = 2.45;
height = 1.9;
distance_cylinder = 24;
distance_pieces = 120;
size_bridge = distance_pieces - distance_cylinder - 3/2*radius_part;
nb_cyl = 4;
height_bridge = 1.9;
depth_bridge = 2;

//CODE

//The first piece is based on two cylinders
module cylinders(height,radius,distance_cylinder)
{
    distance = distance_cylinder;
    union() {
        cylinder(h=height,r=radius,center=true);
        translate([distance,0,0])cylinder(h=height,r=radius,center=true); 
    }
}

//The two cylinders are merged to create a hull, which gives us a single piece
module attaches(height,radius,distance_cylinder)
{
    distance = distance_cylinder;
    hull(){
        cylinders(height,radius,distance);
    }
}

//Holes have to be created in order to fix the piece on another one
module piece_with_holes(height,radius_part,radius_hole,distance_cylinder)
{
    //remove a smaller hull to create a hole
    difference(){
        attaches(height,radius_part,distance_cylinder);
        attaches(height,radius_hole,distance_cylinder);
    }
}

//In order to link 2 lego pieces, we have to duplicate the attaches that will be linked with a bridge later on
module duplicate_attaches(height,radius_part,radius_hole,distance_cylinder,distance_pieces)
{
    union(){
        piece_with_holes(height,radius_part,radius_hole,distance_cylinder);
        translate([distance_pieces,0,0])piece_with_holes(height,radius_part,radius_hole,distance_cylinder);
    }
}

//We need a 'bridge' between the two attaches that is flexible to allow a bend movement
module bridge(width, depth, height, center)
{
    translate([(distance_cylinder+distance_pieces)/2,0,0])cube([width,depth,height], center);
}

complete_flex(height,radius_part,radius_hole,distance_cylinder,distance_pieces, size_bridge, depth_bridge, height_bridge, center=true);

//Then, as with the creation of the hull by the merging of the two cylinders, we here merge the two attaches with the bridge to make it one piece
module complete_flex(height,radius_part,radius_hole,distance_cylinder,distance_pieces, size_bridge, depth_bridge, height_bridge, center)
{
    union(){
        duplicate_attaches(height,radius_part,radius_hole,distance_cylinder,distance_pieces);
        bridge(size_bridge, depth_bridge, height_bridge, center=true);
    }
}