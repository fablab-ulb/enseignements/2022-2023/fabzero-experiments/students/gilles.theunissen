# 3. 3D Printing

This week I 3D printed the FlexLink designed last week with OPENSCAD.

## PrusaSlicer & machine model selection

The software used to export the piece to print it is PrusaSlicer (a free, open source and often updated software that allows the exportation of a 3D model to print it). One should be careful to select the right 3D printer model during the setup. I went too fast and missed the selection of the model which was thus missing when I tried to select it in the "printer" section in the panel. In the upper right part of the window, there is a panel where we must choose the printing settings, the filament and the 3D printer model.

### Printing settings

This section defines the diameter of the material coming out of the extruder as well as the focus on one of the options (here I selected quality).

![](images/printing_settings_2.png)

### Filament

The chosen filament type must match the one used in the 3D printer, here I had generic PLA.

![](images/filament_2.png)

### 3D printer model

And finally I had to select the model of the machine I was going to use.

![](images/printer_2.png)
![](images/prusaslicer_3.png)

### Cut now & verifying layers in PrusaSlicer 

The "cut now" button allows a better understanding of the structure of the piece and the way it's going to be printed. By then playing with the vertical cursor at the right of the window, we can set a treshold on the height of the piece and analyze the structure at each step. One especially interesting layer is the first one, looking at this one allows us to evaluate the filling value we need (in my case, the piece was thin and thus the filling value could be set to 0 (no difference)). By increasing or decreasing the threshold progressively, we can see how the 3D printer will proceed the impression.

### Export the G-code

Whenever the piece is ready to be printed, it's only needed to click on "export the G-code" button and put the file on an SD card.

## Keeping the surface clean

It's important to keep the surface of the printer clean by using the product to suppress any imperfection that could lead to a detachment of the piece during the impression. The first time I did it, I probably did not do it enough because my piece has come off the surface and it ruined the printing (cf. results section).

## Calibration of the first layer

In order to well prepare the impression, one should calibrate the first layer, meaning verify that the first layer will be well printed, because if not, it will already ruin the rest of the printing. In my case, after the first trial, I re-tested the calibration and it was fine, thus telling me it was probably the surface that was not clean.

## Results

Finally, here are the three results. As explained above, the first one has come off the surface during the impression but so has the second one (so I stopped the printing and launched another one after cleaning up again). The last result is satisfying, well filled, robust and flexible.

![](images/results_3.png)

## What I learned

I learned how to use a 3D printer properly (how to keep a clean printing surface to avoid peeling, calibration of the first layer etc.).


