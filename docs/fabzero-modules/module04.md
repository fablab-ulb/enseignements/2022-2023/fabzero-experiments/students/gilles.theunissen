# 4. Selected Fab Tool

This week I learned how to use an Epilog laser cutter. I learned how to design and edit an .svg (vector) file containing the template to be laser cut with the cutter and then send it to the machine. I also learned how to establish safety measures when using the machine and how to set it up correctly.

## Group Assignment

### The design of our cutout with Inkscape (design and modification)

With my teammate, we decided to laser cut a shuriken. We decided to start by using an .svg file from the internet and to modify it to test the different possibilities offered by the machine. The .svg file provided an initially filled room, as below.

![](images/shuriken.png)

We used different colors for each element of our model because later in the Epilog software, we separate the model according to the color in order to make a different treatment by the machine for each element (engraving or cutting) with different parameters. Here is a screenshot of our modified shuriken.

![](images/shuriken_3.png)

For the laser cutter, we need the contour of the shape for cutting but not the filling. So we set the outline in Inkscape and then removed the fill for each of the elements in our model. Once this was done, our modified shuriken was ready to be sent to the Epilog software. 

### The cut in practice (parametrization and adaptation with Epilog software)

### Specificities of the machine:

* Cutting surface : 81 x 50 cm
* Maximum height: 31 cm
* LASER power: 60 W
* Type of LASER: CO2 (infrared)

We used wood scraps for cutting because there were some available next to the cutting machines.

### In the epilog software

![](images/shuriken_result.png)

In Epilog, we separated the different parts of our model according to their color. For the first cut shuriken, we have a text element and a pattern element. 

The text element brings a question: Should we use the engraving mode (which seems rather appropriate for a text element) or the cutting mode? For a text element (red element), it turns out that the cutout mode is more appropriate with certain parameters than the engraving mode. This is because the engraving mode scans "by slice" the concerned pattern and this can lead to a distorted and inaccurate text. On the other hand, the cut mode follows the path of the pattern, which ensures a precise layout. The parameters are important, indeed, a high speed with a low power indicates that the line will not be deep in the part.

![](images/shuriken_text.jpg)

For the shuriken pattern (black element), the cutting mode is chosen and the parameters are reversed with respect to the text cutting (kind of engraving). Indeed, here we take a low speed combined with a high power. This makes the laser go through the material (because we want a cut), but be careful not to set these parameters too high so as not to set the material on fire or to brown it. The number of passes can be changed to make sure you get through the room.

![](images/shuriken_shape.png)

For the second shuriken, we decided to make a bigger one and to use the engrave mode. In order to use the engrave mode, we decided to engrave a small star in our shuriken. The drawing turns out to be precise.

![](images/shuriken_gravure.png)

Finally, the cutting area of the machine must be set correctly to avoid unnecessary movement out of the material with the laser pointer. Please note that any element outside the pink gridded area will not be cut out. Once everything is ready, just click on "print".

![](images/shuriken_zone.png)


### Pre-print calibration

Automatic with the epilog machine.

### Security

#### Before

* Know which is the nature of the material to be cut.
* Always turn on the smoke extractor.
* Know where the emergency stop button is.
* Know where to find a CO2 extinguisher

#### During

* Wear adequate protective eyewear
* Do not stare at the impact of the laser beam
* Stay close to the machine until the cutting process is complete

#### After

* Do not open the machine while there is smoke inside
* Remove residues from the cut (if necessary, use a vacuum cleaner)

## Individual assignment

### The mini-project

The individual assignment we were given consisted of the design of a kirigami and its cutting with the laser cutter. I decided to make a 3D staircase from a 2D sheet. The production process (pipeline) is similar to the one used to make the shuriken. I did the 2D design with illustrator (instead of InkScape) then opened the svg with Epilog (the software used for the Epilog printer) and set the print area and parameters as before. I then checked that the security conditions were respected and launched the printing which lasted about 15 seconds.

The design of the staircase is extremely simple but it took 2 tries to get there because thinking a 3D shape with a 2D drawing was not easy to do. Here is the first try:

### First try

As seen below, the first result shows that some of the cuts were not well proportioned. So I took these mistakes into account to modify the design.

![](images/stairs_first_try.png)

### SVG design 

So I corrected the svg file with illustrator and here is the 2D design (extremely simple).

![](images/stairs_design.png)

The design consists of 5 vector lines that will be cut, these will allow to switch from 2D to 3D (see hero shots at the end).

I decided to use paper for the kirigami because you need to be able to fold the material to have a 3D product. Since paper is quite easily flammable, I decided to use a very low power and a high speed. I started with 1 pass but I was surprised to see that the laser was able to engrave the sheet without even piercing it, so I added an extra pass so that the sheet was pierced. Only the cutting process was necessary here, no engraving.

The cutting parameters were as follows:

* speed: 80%.
* power: 5%.
* frequency: 100%.
* passes: 2

### Second try

The sheet then had to be folded precisely to get the final result.

![](images/stairs_pic1.png)
![](images/stairs_pic2.png)

### What I learned

I learned how to modify/create an svg file with OpenScad (because I already knew how to do it with Illustrator), how to open an svg file with Epilog's software as well as how to ungroup the different elements by color, how to place and set the cutting parameters (engraving or cutting mode, speed, power, etc.), how to cut in complete safety.





