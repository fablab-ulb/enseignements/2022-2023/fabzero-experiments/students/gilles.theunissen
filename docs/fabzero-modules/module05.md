# 5. Group dynamics and final project

This week, I worked on identifying a problem, analyzing its causes and identifying solutions to solve it. I first did this work alone based on the problem tree and solution tree technique (part 1) and then, I proceeded to a group work during which 4 of us worked on the identification of a problem and on the ways to solve it (part 2).

## Part 1 - Project Analysis and Design

Identifying a problem, analyzing its causes and identifying solutions to solve it. In order to do that, I used the problem & solution trees technique. The idea is to first create a problem tree that represents a problem, its causes and the impact of the issue, then to transform this tree into a solution tree that represent a goal, the ideas & activities to achieve it and the impact of the proposed solutions. 

### Problem tree

![](images/problem_tree_2.drawio.png)

### Solution tree

![](images/solution_tree_2.drawio.png)

I chose the environmental impact of the AI industry because as an AI engineer, this topic is something that matters to me. The hype around AI and machine learning is huge but the impact that this industry has on the environment is not discussed enough in my opinion.

## Part 2 - Group formation and brainstorming on project problems

### Method

1. Everyone brought an object that represents a thematic that is important to them.
2. We formed groups of 4-5 people based on each person's object.
3. We took a few minutes to write words on a sheet of paper that we thought linked our objects.
4. Then, we each wrote a thematic that was important to us on a post-it note, discussed what motivated us in this thematic and chose one of them to move on to the next step.
5. The thematic chosen in the previous step served as a starting point for a process of generating problematics that flowed from it.
6. Finally, a member of the group was sent to gather problematics for our thematic from other groups. During this time, the rest of the group stayed behind to welcome the other "ambassadors" and to propose problematics arising from their theme.

### My object 

My object is a reusable water bottle. I chose this object because it represents recycling to me. Environment is a very important topic to me so it seemed logic to bring an object related to it.

![](images/gourde.jpg)

### My group 

My group is made up of members who all brought a gourd for environmental reasons, so we all found ourselves discussing issues that speak to us. Something interesting is that we are 4 students from different faculties or options. One student is in biology, one is in computer science, one is in electromechanical engineering, and I am in artificial intelligence engineering. So we all have different backgrounds but are interested in the same types of problems.

### Links between our objects & thematic of each member of the group 

We took a few minutes to write words on a sheet of paper that we thought linked our objects. Then, we each wrote a thematic that was important to us on a post-it note, discussed what motivated us in this thematic and chose one of them to move on to the next step. Not surprisingly, the links that emerged were mainly water, recycling, ecology, portability etc.

![](images/thematique.jpg)


### Global thematic for the problematics generation process

At first, we had a hard time choosing a thematic and went straight to problematics. We ended up choosing recycling in general as a thematic. Since we did not manage to find relevant problematics in the time allowed (we started saying anything hoping to stimulate ideas), I describe here the process / reasoning to have to go from a thematic to a set of problematics.

The idea was to take a concrete example of a problem related to recycling such as "the environmental impact of the production of plastic bottles".  From there, we had to derive a problematic from this theme. The path is as follows. 
1. Why is the production of plastic bottles a problem for the environment? Because it pollutes.
2. What does it mean to pollute? Emitting CO2 emissions, physically polluting the oceans with plastic etc.
3. Why is polluting the oceans with plastic a problem? Because micro plastics kill fish.
4. Etc.

The recursivity of the "why? and "how?" questions made it possible to bring the theme "the environmental impact of plastic bottle production" (not a question) to the problematic "how to reduce the presence of micro plastics in the oceans ?" (a question).

### Ideas following exchanges with the other groups

![](images/ambassadeur.jpg)

Here is the list of "problematics" that we collected while discussing with the other groups following the presentation of our thematic. The results were very diverse.

### Potential problems at the end of the process

At the end of the process, we concluded that we had to be careful not to go in circles on certain ideas or get too deep into one problematic without keeping an open mind on other ideas, which was difficult because we were all very focused on the same topic.

### The brainstorming process

Brainstorming was not an easy task. I think it is because all the group members globally brought the same object, maybe there was not enough confrontation or variation of ideas, which led to a low stimulation of ideas during the process. We also felt that there was not enough time to achieve a good result. However, we found it amusing and rectified it when the last person in the group arrived at the next class and we managed to see the goal better, so we have a better result as a basis for our project. We found a convergence following our 'out of the box' discussions towards a problematic that matters to us all.

## Part 3 - Group dynamics

Group dynamics is important so that the meetings and the project go well, so we had a course which consisted of presenting us with the tools/methods to put in place before starting to work in a group. Here are 3:

1. Role at meetings:

Giving roles to the project members allows to gain efficiency, to save time and to better organize the meetings.

- facilitator
- secretary
- time manager

The facilitator is in charge of putting structure in the meeting by linking the points to be discussed and by putting dynamism. The secretary is responsible for documenting the meeting to keep track of it and to note down future tasks and roles. The timekeeper is responsible for informing the group members when the timer indicates the end of the speaking rounds/topics etc.

2. Weather:

Weather here refers to a technique of asking about everyone's mood at the beginning of the meeting and at the end of the meeting. This allows everyone to inform and be informed of the mental state of the members so that they can take it into account during the meeting. By doing this, the meeting is likely to go better.

3. Decision-making methods:

- consensus (everyone)
- majority judgment/majority (the majority of people)
- weighted/ranked voting (assigning weights to decisions to establish a sorted preference)
- random draw (chance)
- objection 
- criteria

Choosing how to proceed with decisions is a matter of governance. The group should work out how to make decisions knowing the different methods available.