# 1. Project management and documentation

This week I worked on verifying that some useful components for the future of the course are correctly set up and learned about project project management. But before getting into it, let's present myself.

## Who am I ?

My name is Gilles Theunissen, I'm a student in the second year of a master in Polytech, computer engineering (AI & optimization). I'm 23 years old and I chose this course in order to learn how to use the machines available in the FabLab in order to realize my own small projects.

## The tools setup

This section is dedicated to the set up of tools that are really useful when working in groups on projects that need code or documentation. First, we will setup the right shell, then install a package manager, install and setup GIT, log in or register in GitLab, and setup an SSH key. Finally, we will learn about cloning and versioning with GIT.

### Command lines

The command line interface (prompt) is an interface that allows the user of a computer to communicate with him in code. This tool is very useful to do an infinite number of things such as moving in the computer file hierarchy, creating a folder or a file, compressing an image, downloading libaries etc. Each operating system has his default prompt and I am on MacOs, so the following explanation will be centered around this operating system. MacOS is a UNIX family system, which means that UNIX family shells are availables and can be selected in the command line. One of those shells is BASH and widely used, it is well known and thus it is relatively easy to get used to it. With a few command lines in the base terminal I could select the Bash shell.

|  instruction    |  command            |
|-----------------|-------------------  |
| open terminal   |                     |
|                 | $ cat /etc/shells   |
|                 | $ /bin/bash         |
|                 | $ chsh -s /bin/bash |
| close terminal  |                     |

### Package Manager

A package manager is "a collection of software tools that automates the process of installing, upgrading, configuring, and removing computer programs for a computer in a consistent manner". Multiple package managers exist such as pip, conda and HomeBrew (brew) (mac). I used brew for installing git (cf. following question). To install HomeBrew, I used the following command.

|  instruction    |  command           |
|-----------------|--------------------|
| open terminal   | |
|   | $ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" |
| close terminal  | |

### GIT

GIT is an informatic tool used for source code management. It allows anyone to manipulate multiple versions of a code project and offers many possibilities to collaborate.

|  instruction    |  command           |
|-----------------|--------------------|
| open terminal   | |
| install git with the package manager  | $ brew install git |
| close terminal  | |

### GitLab

GitLab is a web-based Git repository (based on git explained above) that provides the possibility to create and access repositories as well as the possibility to handle and trace issues. The idea of using GitLab in this course is to keep up to date a blog anyone could follow in order to reproduce the current project. To use GitLab, one should register or log in [here](https://gitlab.com/users/sign_up)

### SSH key

In order to communicate safely between the local machine (the laptop, in my case a mac device) and the remote server, we have to establish a secure connection. To do that, we generate an SSH key which allows encryption and decryption of any transmitted message (in both ways). This key is assigned to one computer (this one) and is linked to one GitLab account. In my case, I already had an SSH key connected to my account so I only had to verify everything has been correctly done. The following commands allows to check if an SSH key is already generated and copies it.

|  instruction           |  command                            |
|------------------------|------------------------------       |
| open terminal          |                   |
| go to the SSH folder         | $ cd ~/.ssh/                        |
| if folder exists       | $ ls                                |
| to copy the public key | $ pbcopy < ~/.ssh/id_rsa.pub        |
| close terminal  | |

Then go to your GitLab account and go to Profile -> Préferences -> SSH Keys and copy it if it's not already present in the "Your SSH keys" section. Once it is done, your account and your machine will safely be connected. Note that some algorithms are better than other for the security. For example : ED25519 is currently the most secure encryption option. RSA is also widely used. Choosing the encryption scheme is up to you.

### Clone and version control

Now that the connection is established, we have to clone our GitLab respository. It basically means making a copy of the state of the project to our computer while having the possibility to keep it updated on the remote side. In order to to that, I followed the instructions below. Here "open terminal" means open it at the location where you want to have your project on your computer, it will then be the root of the project.

In order to clone the repository to your computer, open it on GitLab, click on clone and copy the link below "Clone with SSH".

CLONE

|  instruction    |  command                                        |
|-----------------|-------------------------------------------------|
| open terminal  | |
| clone the code | $ git clone "clone with SSH link" |
| close terminal  | |

KEEPING THE REMOTE SIDE UP TO DATE

|  instruction    |  command                                                          |
|-----------------|-------------------------------------------------------------------|
| open terminal   |                                                                   | 
| pull the code from the remote version   | $ git pull                                                        | 
| add the files to the transfer  | $ git add -A or git add .                                                     |
| give a description of the update| $ git commit -m "COMMENT TO DESCRIBE THE INTENTION OF THE COMMIT" |
| push the code to the remote side  | $ git push                                                        |
| close terminal  | |

**Note** : When pushing, one should be in the arboresence of the root of the git (the place in the computer files hierarchy where git knows that all files beyond in the hierarchy are governed by the git versioning). If one want to be sure to update all files from the root, one should be at the top of the hierarchy under th git versioning (meaning the root !). If one want to modify only files that are lower in the hierarchy, go to the folder itself in the terminal and push from there, this won't modify files outside the considered folder.

## Documentation

A key for having a step back on the way of doing things (getting a picture of the whole process from scratch) is by documentating it. It means writing in a structured manner the way of setting up or installing something ... In order to do that, we can write the content in a ".md" (Markdown) file. Markdown is a lightweight markup language (add symbols into a text file to give it a structure). To create the blog you are currently reading, a ".md" file was thus created inside the folder of my repository to ensure the update of the file.

#### Useful Markdown's commands and syntax

The syntax of a markdown file for the use I need can be regroup as the following set of tools :

* Writing in italic can be done by using asterics around the word *word*
* Writing in bold can be done by using double asterics around the word **word**
* Writing code as text can be done with tildas around the word `word`
* A star * is a dot when displayed on the blog.
* Inserting a link referenced and hiden by a word can be done with "bracket here bracket(link)"
* There is a hierarchy of title : # (big title), ## (smaller title), ### (even smaller title), etc.
* A table can be displayed with : |- |- | style syntax (for more go see : https://www.markdownguide.org/basic-syntax/)
* To insert an image : "!bracket alt bracket(docs/images/file.extension)" where "docs/images/file.extension" is the relative path (meaning that it gives the place where the file is located according to where we are).

Since I use several image formats, it can be useful to resize an image to the desired dimensions. I used the following command (in the terminal that I open where the images are located): 

convert myfigure.png -resize sizexsize myfigure.png

To keep storage low, having images that are less heavy is important. On max, there is a pre-installed image processing library **sips** that allows us to compress our images with the following command : 

sips -s format jpeg -s formatOptions low <biggerFile>.png --out "./<smallerFile>.jpeg"

**Note** : PNG files are heavy, so we can switch the format of the image. For more informations, here is the webpage : [here](https://rachelrly.medium.com/how-to-compress-images-in-the-mac-terminal-57f8ddd11926)

The website you are currently reading has been done using those commands.

## Project management

The following link : http://fablabkamakura.fabcloud.io/FabAcademy/support-documents/projMgmt/#as-you-work-documentation talks about useful principles in the context of project management. What I retained is that documentation is time consuming and will be better done and quicker if it is done as soon as the content is learned. Each week, I will document the current week (not especially in a perfect way but set the base of the content to be sure to be complete) and then later on, I will be completing the documentation.

The following formulas are to keep in mind :

* Parkinson Law : “Work expands so as to fill the time available for its completion.”
* Hofstadter’s Law : “it always takes longer than expected, even taking into account Hofstadter’s Law.”

## What I learned

I learned how to check that tools like GIT were installed, that my SSH key existed and was linked to GitLab, how to use Markdown syntax and tools, how to resize and compress images and I learned about project management techniques.

