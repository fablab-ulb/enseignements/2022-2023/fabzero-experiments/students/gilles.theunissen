# 2. Computer-aided design (CAD)

This week I designed a fully parametrized FlexLink with OPENSCAD.

## What is a FlexLink ? 

A FlexLink is a 3D printed piece that allows to create compliant mechanisms (it is a flexible mechanism that achieves force and motion transmission through an elastic body deformation). The small piece developed here was inspired by FlexLinks developed in collaboration with Lego (dimensions match). It is a piece made up of 2 attachments connected by a flexible link.

## What is OpenScad ?

OpenScad is a free software that allows the users to create a parametrized 3D modelisation, it comes very useful when the project has to be scaled, rotated, translated for example. This software allows the creation of geometrical forms and furnishes mathematical operator such as "union" & "difference" as well as other more complex functions such as "hull" (cf. bellow).

## How to proceed ?

In order to create this FlexLinks, I asked myself multiple questions and ended up with a work plan. Such quesions are : 

    - How to contruct the piece ?
    - How to ensure that everything is going well during the process (control of quality during the process) ?
    - How to have a maintenable/minimal & reusable/parametrized code ?

## How to contruct the piece ?

I chose to design a simple FlexLink to focus on developing a good pipeline. As OpenScad allows the users to use the "union" operator, I was able to design smaller pieces (way easier to create) and then put them together. The design I was looking for is the following one.

![](images/target_piece_scaled.png)

Framework followed :

![](images/flow_creation_flexlink_5.drawio.png)

### A) The final piece will be the union of smaller pieces, in which order should the pieces be created ?

I chose to start by creating the attaches (cf. next subsection) and then design the link between the two attaches but the opposite is acceptable too. My mistake was not to center the piece on the link immediately, thus potentially complicating the following transformations (several translations and the formula ensuring the independence between the parameters (fully parametrized)).

### B) How to contruct the sub-pieces ?

The attaches are the same, so once I designed one, I could replicate it (we don't want to do the job twice). The attach is created based on a cylinder. I created a module that creates the two cylinders according to three parameters.

```
//The first piece is based on two cylinders
module cylinders(height,radius,distance_cylinder)
{
    distance = distance_cylinder;
    union() {
        cylinder(h=height,r=radius,center=true);
        translate([distance,0,0])cylinder(h=height,r=radius,center=true); 
    }
}
```
Where height, radius and distance_cylinder are respectively the height of the cylinders, their radius and finally the distance between the two.

```
//The two cylinders are merged to create a hull, which gives us a single piece
module attaches(height,radius,distance_cylinder)
{
    distance = distance_cylinder;
    hull(){
        cylinders(height,radius,distance);
    }
}
```
The hull operation allows the "fusion" of the two pieces. It is like taking a plane along each axis (x,y and z) and moving them through the cylinders. When the plane intersects with both cylinders, the area covered by the plane is filled. In this way, the space between the two cylinders is filled. I first tried by making an union of two cylinders and a cube to make the shape but it required 3 objects instead of two by using the hull.

![](images/hull.png)

Next, I created a smaller similar piece that I extracted (with the difference operator) from the first one to make a hole in it. This finishes the first piece.

```
//Holes have to be created in order to fix the attaches to a lego
module piece_with_holes(height,radius_part,radius_hole,distance_cylinder)
{
    //remove a smaller hull to create a hole
    difference(){
        attaches(height,radius_part,distance_cylinder);
        attaches(height,radius_hole,distance_cylinder);
    }
}
```
Where radius_part and radius_hole are respectively the exterior radius of the attach and the interior radius of the attach (size of the hole). It is obvious that the size of the hole should be strictly smaller than the exterior one.

Next, I had to replicate the attache to have two attaches distant from each other. The union operator transforms two pieces into one unique piece.

```
//In order to link 2 lego pieces, we have to duplicate the attaches that will be connected with a link later on
module duplicate_attaches(height,radius_part,radius_hole,distance_cylinder,distance_pieces)
{
    union(){
        piece_with_holes(height,radius_part,radius_hole,distance_cylinder);
        translate([distance_pieces,0,0])piece_with_holes(height,radius_part,radius_hole,distance_cylinder);
    }
}
```
Where distance_pieces is the distance between the two attaches.

Once the attaches were finished, I designed the link (called bridge here). This bridge is a simple stretched and translated cube.

```
//We need a 'bridge' between the two attaches that is flexible to allow a bend movement
module bridge(width, depth, height, center)
{
    translate([(distance_cylinder+distance_pieces)/2,0,0])cube([width,depth,height], center);
}
```
Where width, depth and height are the parameters of the cube. The formula in the translate function ensure that the bridge fit accordingly to the change of the other parameters.

```
//Then, as with the creation of the hull by the merging of the two cylinders, we here merge the two attaches with the bridge to make it one piece
module complete_flex(height,radius_part,radius_hole,distance_cylinder,distance_pieces, size_bridge, depth_bridge, height_bridge, center)
{
    union(){
        duplicate_attaches(height,radius_part,radius_hole,distance_cylinder,distance_pieces);
        bridge(size_bridge, depth_bridge, height_bridge, center=true);
    }
}
```
Where size_bridge, depth_bridge and height_bridge are the width, depth and height parameters defined above.
Finally, the value of the parameters defined across this section are all set in the beginning of the code :

```
$fn = 50;
radius_part = 5.5;
radius_hole = 2.45;
height = 1.9;
distance_cylinder = 24;
distance_pieces = 120;
size_bridge = distance_pieces - distance_cylinder - 3/2*radius_part;
nb_cyl = 4;
height_bridge = 1.9;
depth_bridge = 2;
```

Wher $fn is the number of faces we want each of our pieces to have. The full code is available [here](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/gilles.theunissen/-/tree/main/docs/fabzero-modules/files/week%203/my%20code)

### C) How to handle transformations such as translation & rotation across multiples pieces ?

This subsection refers to a problem evoked in the subsection A). I experienced that multiple transformations can become a challenge when the objective is to make the project fully parametrized since I had to understand the relative transformations between the pieces. So what I did was to minimize the number of transformations applied to the pieces in order to be able to, for example, make sure that the links would connect well to the attaches if I changed the size of the attaches or if I changed the link size.

## How to ensure that everything is going well during the process (control of quality during the process) ?

To ensure that each part of the code worked well, I called each module, each time it was coded, setting values to the parameters. When the code starts to be quite long, we don't want to search in the whole code for an error we could have found sooner.

## How to have a maintenable/minimal & reusable/parametrized code ?

Having a maintenable code is essential in order to work with someone else on a project, it allows colleagues to understand what has been done. A typical good practice is to keep the function quite short (5-15 lines), this way it's easier to debug and easier to read (each function has it's specific task), going through the code is done faster. Keeping a code minimal means don't write code lines that could be avoided, it's easier to process. Keeping the code reusable means coding modules/functions that could be reused in another part of the code, that's why keeping the functions minimals can be useful. Finally, the user of a program should be able to easily scale/translate/rotate the 3D model by changing a few parameters value and not going through the whole code and changing values multiple times, thus increasing the chances of making a mistake.

## Licensing

### License on my code

When publishing a code on a site such as GitLab or GitHub, putting a license on it is a good thing. Here, I used the 'Creative Commons License' which allows people in possession of the code to reuse it to a certain extent and according to certain rules. I chose the 'Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0]' license which lets people do what they want with the code as long as they follow these 2 rules : Credit must be given to the creator, Adaptations must be shared under the same terms. More informations are available [here](https://creativecommons.org/licenses/by-sa/4.0/).

```
FILE : code.scad

AUTHOR : Gilles Theunissen <gilles.theunissen@ulb.be>

DATE : 08-03-2023

LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
```

### Reusing the code of someone else

Since I used and modified (I only modified the scale factor for the purpose of citing) the code of one of my colleague, I have to cite him according to the license on his code. I must give him credits and share my adaptation under the same terms. The modified code is available [here](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/gilles.theunissen/-/tree/main/docs/fabzero-modules/files/week%203/my%20code)

![](images/flexlink_morgan.png)

```
// File : modified_morgan_code.scad
// Author : Gilles Theunissen
// Date : 08-03-23
// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
// Credits :  The original work is attributed to Morgan Tonglet, shared under the CC BY-SA license. 
// The original version of this file can be found [here](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/morgan.tonglet/-/blob/main/docs/fabzero-modules/files/module3/mod3_flexilink001.scad)
```
### What I learned

I learned how to use OpenScad, how to pareameterize a 3D part and learned that having a clear production process is extremely important to save time. It is important to pare the part progressively and completely in order to avoid mistakes when modifying the piece.

