## Foreword

Hello!

Welcome to my blog where you can find information about me and my work. Have a nice reading!

## About me

![](images/photo_of_me.jpg)

Hi, my name is Gilles THEUNISSEN, and I'm in my last year of a civil engineering course at ULB, specializing in artificial intelligence. I'm an academic, but I'm also involved in a student club called the Cercle Polytechnique, for which I organize events such as concerts, team building, etc., and for which I'm in charge of audiovisuals, because my passion is... audiovisuals!

## My background

I'm Belgian, I've lived in Rhode-Saint-Genèse most of my life but now live in Ixelles to be closer to the ULB. I decided to study engineering because I love maths and science, then I discovered computer science in a more advanced way and decided to take up this option (AI option) because I love the idea of being able to develop anything just with a computer (a product, a service, a game etc.) especially with an algorithmic dimension. I love computers, but my passion is music. I'm a guitarist, singer and dj. I spend my life composing and living incredible moments thanks to it. As it happens, however, I'm not a very "hands-on" person, so I decided to take an interest in creative machines, particularly 3D printers. That's why I chose this course, to finally get my hands dirty.

## Previous work

I've had some particularly rewarding experiences. The first was my trip to India in 2014, which showed me a beautiful country and where I visited a fablab in the center of Mumbai where brilliant people work. It inspired me, even if it took me a while to finally touch those machines! I was also project leader for a group of BA1 students last year, guiding them through their project to transform mechanical energy into electrical energy by harvesting the vibratory energy of walls. My experience as audiovisual manager of an ASBL also taught me a lot about team and resource management.
